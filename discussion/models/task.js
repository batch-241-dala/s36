// Create the Schema, model and export file

const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({

	name: String,
	status: {
		type: String,
	default: "pending"
	}

})

// "module.exorts" is a way for Node JS to treat this value as a "package" that can be used by other files
module.exports = mongoose.model("Task", taskSchema);
