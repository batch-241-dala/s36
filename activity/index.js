// Setup the dependencies
const express = require('express');
const mongoose = require('mongoose');

const taskRoute = require('./routes/taskRoute')

// Server setup
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Database Connection
// Connecting to MongoDB Atlas
mongoose.set('strictQuery', false);
mongoose.connect("mongodb+srv://admin123:admin123@cluster0.me7rc81.mongodb.net/s36?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})


let db = mongoose.connection;
db.on('error', console.error.bind(console, "Connection Error"));
db.once('open', () => console.log('Connected to MongoDB Atlas'));
// Add the task route
// Allows all the task routes created in the "taskRoutes.js" file to use "/tasks"
app.use("/tasks", taskRoute);


























app.listen(port, () => console.log(`Now listening to port ${port}`));
