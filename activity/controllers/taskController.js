// Controllers - contains the functions and business logic of our Express JS application. All the operations it can do will be placed in this file

// Uses the "require" directive to allow access to the "Task" model which allows us to access the methods to perform CRUD operations.
// Allows us to use the contents of the "task.js" file in the "models" folder
const Task = require("../models/task");

// Controller function for GETTING ALL THE TASKS
// Defines the functions to be used in the "taskRoutes.js" file and export these functions

module.exports.getAllTasks = () => {
	
	// The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client/Postman
	return Task.find({}).then(result => {

		return result;

	});
};

module.exports.createTask = (reqBody) => {

	// Creates a task object based on the Mongoose Model "task"
	let newTask = new Task ({
		// Sets the "name" property with the value received from the client
		name: reqBody.name
	});

	// the first parameter will store the result return by the Mongoose save method
	// the second parameter will store the "error" object
	// The "save"
	return newTask.save().then((task, error) => {
		if(error) {
			console.log(error)
			return false
		} else {
			return task
		}
	});

};

module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
		if (error) {
			console.log(error)
			return false
		} else {

			return removedTask
		};


	});

};

module.exports.updateTask = (taskId, taskBody) => {

	return Task.findById(taskId).then((result, error) => {
		
		if (error) {
			console.log(error)
			return false
		} else {

			result.name = taskBody.name

			return result.save().then((updatedTask, saveErr) => {
				if(saveErr) {
					console.log(saveErr)
				} else {
					return updatedTask
				}
			});

		};
	});

};

// get specific task

module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then((result, err) => {
		if(err) {
			console.log(err)
			return false
		} else {
			return result
		}
	})
}

// change task status

module.exports.updateStatus =(taskId, taskBody) => {

	return Task.findById(taskId).then((result, error) => {
		if (error) {
			console.log(error)
			return false
		} else {
			result.status = taskBody.status

			return result.save().then((update, err) => {
				if(err) {
					console.log(err)
				} else {
					return update
				}
			})
		}
	})

}